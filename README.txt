***Olink Target 96 Inflammation panel data and R code for Earlyfit PRESTO Immune study***

***Title: ‘Identification of potential inflammation markers for outgrowth of cow’s milk allergy’***

***Creators***
Diana M Hendrickx 1, Mengyichen Long 1, PRESTO study team *, Harm Wopereis 2, Renate G van der Molen 3, Clara Belzer 1
1 Laboratory of Microbiology, Wageningen University, Wageningen, The Netherlands
2 Danone Nutricia Research, Utrecht, The Netherlands
3 Department of Laboratory Medicine, Radboud UMC, Nijmegen, The Netherlands

* Correspondence: clara.belzer@wur.nl

* PRESTO study team: A list of authors and their affiliations will be published together with the manuscript.

***Related publication***
Publication pending

***General Introduction***
This folder contains the Olink Target 96 Inflammation panel data and R scripts for the statistical analysis within the Earlyfit PRESTO Immune study.
This study was part of the EARLYFIT project (Partnership programme NWO Domain AES-Danone Nutricia Research), funded by the Dutch Research Council (NWO) and Danone Nutricia Research (project number: 16490).

***Data availability***
Clinical data are available from Danone Nutricia research upon reasonable request (contact: Harm Wopereis, 
Danone Nutricia Research, Utrecht, The Netherlands, Harm.Wopereis@danone.com).
