npx_data_presto.xlsx: file with data from Olink Target 96 Inflammation panel. 

The data has the following columns: SampleID, Index, OlinkID, UniProt,Assay, MissingFreq,
Panel, Panel_Version, PlateID, QC_Warning, LOD, NPX.

An explanation of these columns is provided on the website of the OlinkAnalyze package:
https://cran.r-project.org/web/packages/OlinkAnalyze/vignettes/Vignett.html

The columns "subject", "visit", "allergy_status" and "treatment" are part of the clinical data 
and available on reasonable request (contact: clara.belzer@wur.nl).

